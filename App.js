import React, { useState } from 'react'
import { 
  View, 
  Text, 
  StyleSheet, 
  ScrollView, 
  RefreshControl, 
  Alert, 
  FlatList, 
  SectionList 
} from 'react-native'
import Exemple from './exemple';


export default function App() {


  return (
    <View style={styles.container}>
      <Exemple/>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    
  },
  viewList: {
    fontSize: 10,
    backgroundColor: "cyan",
    justifyContent: "center",
    alignItems: "center",
    margin: 5,
  
  },
  listContent: {
    fontSize: 15,
    color: "black",
    padding: 10,
  },
  listContentData: {
    color: "silver", 
    padding: 5,
    textAlign: "center",
  }  
});

// ---- sectionList---------------------------------
// const obj = [
//   {
//     role: "Père",
//     data: ["john", '45']
//   },
//   { role: "Mère",
//     data: ["jane", '35']
//   },
//   { role: "Fils",
//     data: ["jack", '15']
//   },
//   { role: "Fille",
//     data: ["jill", '10']
//   },
//   { role: "Chien",
//     data: ["Gluglu", '5']
//   },
// ]

// const [family, setFamily] = useState(obj)

//   return (
//     <View style={styles.container}>
//       <SectionList
//         sections={family}
//         // keyExtractor={(item, index) => index.toString()}
//         renderSectionHeader={({ section }) => (
//           <View style={styles.viewList}>
//             <Text style={styles.listContent}>Rôle: {section.role}</Text>
//           </View>
//         )}  
//         renderItem={({ item }) => {
//           return (
//             <View>
//               <Text style={styles.listContentData}>Name: {item[0]} | Age: {item[1]}</Text>
//             </View>  
//           )
//         }}
          
//       />
//     </View>
//   )
// }



// ---- FlatList horizontale & inverted---------------------------------  
//   const obj = [
//     {name: "John", age: 20 },
//     {name: "Jane", age: 30 },
//     {name: "Joe", age: 40 },
//     {name: "Jack", age: 50},
//     {name: "Jill", age: 60},
//     {name: "soko", age: 60,}
//   ]
//   const [family, setFamily] = useState(obj)
//   const [invert, setInvert] = useState(false)

//   const onRefresh = () => setInvert(!invert);

//   return (
//     <View style={styles.container}>
//       <FlatList
//         data={family}
//         renderItem={({item}) => {
//           console.log(item)
//           return (
//             <View style={styles.viewList}>
//               <Text style={styles.listContent}>Name: {item.name} | Age: {item.age}</Text>
//             </View>
//           )
//         }}
//         keyExtractor={(item, index) => index.toString()} // visiblement ça fonctionne sans ...
//         horizontal
//         inverted={ invert }
//         refreshControl={
//           <RefreshControl
//             refreshing={ false }
//             onRefresh={ onRefresh }

//           />
//         }
//       />
//     </View>  
//   )  
// }



// ---- FlatList verticale ---------------------------------
// return (
//   <View style={styles.container}>
//     <FlatList
//       data={family}
//       renderItem={({item}) => {
//         console.log(item)
//         return (
//           <View style={styles.viewList}>
//             <Text style={styles.listContent}>Name: {item.name} | Age: {item.age}</Text>
//           </View>
//         )
//       }}
//       keyExtractor={(item, index) => index.toString()} // visiblement ça fonctionne sans ...
//     />
//   </View>  
// )  
// }


// ---- ScrollView & RefreshControl ---------------------------------

  // const [refresh, setRefresh] = useState(false)

  // const onRefresh = () => {
  //   setRefresh(true)
  //   Alert.alert(
  //     "info",
  //     "La liste à été rafraichie",
  //     [
  //       {
  //         text: "OK",
  //         onPress: () => console.warn("La liste à été rafraichie"),
  //         style: "cancel"
          
  //       }
  //     ]
  //   );
  //   setRefresh(false)
  // }

//   return (
//     <View style={styles.container}>
//       <ScrollView
//         refreshControl={
//           <RefreshControl
//             refreshing={ refresh }
//             onRefresh={ onRefresh }
//             colors={["red"]}
//           />
//         }
//       >
//       {
//         family.map(member => {
//           return (
//             <View key={member.id} style={styles.viewList}>
//               <Text style={styles.listContent}>Name: {member.name} | Age: {member.age}</Text>
//             </View>
//           )
//         })  
//       }
//       </ScrollView>
//     </View>  
//   )  
// }


