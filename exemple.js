import { TextInput, StyleSheet, Keyboard } from 'react-native'
import React, { useEffect } from 'react'

const Exemple = () => {

    useEffect(() => {
        Keyboard.addListener('keyboardDidShow', _keyboardDidShow)
        Keyboard.addListener('keyboardDidHide', _keyboardDidHide)

        return () => {
            Keyboard.removeAllListeners('keyboardDidShow', _keyboardDidShow)
            Keyboard.removeAllListeners('keyboardDidHide', _keyboardDidHide)
        }

    }, []);

    const _keyboardDidShow = () => {
        alert('veuillez saisir un texte');
    }

    const _keyboardDidHide = () => {
        alert('merci pour votre saisie');
    }

    return <TextInput style={styles.input} 
        placeholder="Saisir un texte"
        onSubmitEditing={() => Keyboard.dismiss()}
    />
}

const styles = StyleSheet.create({
    input: {
        margin: 60,
        padding: 10,
        borderWidth: 1,
        borderRadius: 10,
        backgroundColor: 'yellow',
    }
})

export default Exemple;